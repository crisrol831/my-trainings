:: run robot with specific testcase
:: pybot -t Add_Item_In_Todo Todo_app_test.robot

:: run robot with multiple testcase
:: pybot -t "Test 1" -t "Test 2" Sample2.robot

:: run robot with testcase start with
:: pybot -t Test* Sample2.robot

:: run multiple suites with output dir
:: pybot -t * -d results Todo_app_test.robot Sample2.robot

:: run testcases with tags
:: pybot -t * -i robot-101 Todo_app_test.robot Sample2.robot

:: run testcases but exclude test with tags 
:: pybot -t * -i robot-101 -e obsolete Todo_app_test.robot Sample2.robot