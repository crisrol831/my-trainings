*** Settings ***
Test Setup          Say Hi
Test Teardown       Say Hello

*** Variables ***
&{array}    a=K    b=I    c=l   d=I   e=g
${word}

*** Testcases ***
Test 1
    [Tags]    robot-101    obsolete
    [Documentation]    This is KS test automation using robot
    :FOR    ${value}    IN   @{array}
    \    ${word}=   Evaluate    '${word}'+'${array['${value}']}'
    Should Be Equal    ${word.upper()}    KILIG
    

Test 2
    ${pangram}    Set Variable    The big brown fox jumps over the lazy dog.
    ${len}    Get Length    ${pangram}
    Run Keyword If     ${len}==42    Interlude
    Comment    This do nothing
    # [Teardown]    

*** Keywords ***
Interlude
    Log To Console    \n\n Are you having fun? \n
    Sleep    5s

Say Hi
    Log To Console     \n Hi!

Say Hello
    Log To Console    \n Hello!